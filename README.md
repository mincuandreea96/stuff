# stuff

DIFFERENCE BETWEEN TWO DOUBLES 
1. ABS method
  //case 1
        double d1 = -0.0d;
        double d2 = 0.0d;
        System.out.println(d1);
        
        System.out.println(Math.abs(d1-d1)==0);//printing true , which is correct
       
        //case 2
        double d3 = Double.NaN;
        double d4 = Double.NaN;
        
        System.out.println(Math.abs(d3-d4)==0);//printing false , which is correct
        
        
        //case 3
        double d5 = -2.0d;
        double d6 = 2.0d;
        System.out.println(Math.abs(d5-d6)==0); //printing false , which is correct
        
        //case 4
        double d7 = 2.22d;
        double d8 = 2.22d;
        System.out.println(Math.abs(d7-d8)==0); //printing true , which is correct
        
2. BigDecimalMethod
3.  //case 1
        double d1 = -0d;
        double d2 = 0.0d;
        System.out.println(d1);
        
        
        BigDecimal b1 = new BigDecimal(d1);
        BigDecimal b2=new BigDecimal(d2);
        System.out.println(b1);

        System.out.println(b1.compareTo(b2)==0);     //printing true, which is correct
        
       
        //case 2
        double d3 = Double.NaN;
        double d4 = Double.NaN;
        
        
        BigDecimal b3 = new BigDecimal(d1);
        BigDecimal b4=new BigDecimal(d2);
        System.out.println(b3.compareTo(b4)==0); // printing true , which is not correct
        
        //case 3
        
        //case 3
        double d5 = -2.0d;
        double d6 = 2.0d;
        BigDecimal b5 = new BigDecimal(d5);
        BigDecimal b6=new BigDecimal(d6);
        System.out.println(b5.compareTo(b6)==0);//printing false, which is correct
        
        //case 4
        double d7 = 2.22d;
        double d8 = 2.22d;
        BigDecimal b7 = new BigDecimal(d7);
        BigDecimal b8=new BigDecimal(d8);
        System.out.println(b7.compareTo(b8)==0);//printing true, which is correct
        